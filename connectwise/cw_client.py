import requests
import json

url = "https://api-staging.connectwisedev.com/v2020_2/apis/3.0/company/companies/4"
URL = "https://api-staging.connectwisedev.com/v4_6_release/apis/3.0"
AUTH = {
  'Authorization': 'Basic a3Jha2JvdF9mK0hSZ244U2Jvc0JGZ05neFQ6QTVvdlJSUm5ON3c2WGN1Zw=='
}

payload = {}
headers = {
  'clientId': 'a512791c-d488-4e2d-bbbf-86e35de42b55',
  'Authorization': 'Basic a3Jha2JvdF9mK0hSZ244U2Jvc0JGZ05neFQ6QTVvdlJSUm5ON3c2WGN1Zw=='
}

response = requests.request("GET", url, headers=headers, data = payload)

#print(response.text.encode('utf8'))
print(json.loads(response.text.encode('utf8')))   # this is returning in valid json ' vs " False vs false, etc
