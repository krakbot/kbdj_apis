import requests
import json
from connectpyse.company import companies_api

URL = "https://api-staging.connectwisedev.com/v4_6_release/apis/3.0"
AUTH = {
  'Authorization': 'Basic a3Jha2JvdF9mK0hSZ244U2Jvc0JGZ05neFQ6QTVvdlJSUm5ON3c2WGN1Zw=='
}

c = companies_api.CompaniesAPI(url=URL, auth=AUTH)

print(c.get_companies_count())

#print(response.text.encode('utf8'))
#print(json.loads(response.text.encode('utf8')))   # this is returning in valid json ' vs " False vs false, etc
